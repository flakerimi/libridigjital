<?php namespace App\Helpers;

use Auth;
use App\Answer;

/**
* Helper class
*/
class General
{
	
	public static function test_results($resultCollection)
	{

		$TestResults = \App\TestResult::where([['test_id','=',$resultCollection->test_id],['user_id','=', Auth::user()->id]])->get();   
		$correct_answers = Answer::where([['test_id','=',$resultCollection->test_id],['correct_answer','=',1]])->get(); 
		foreach ($TestResults as $testkey => $testvalue) {
			foreach ($correct_answers as $correctkey => $correctvalue) {
				echo $testvalue->answer_id;
			}
		}
		print_r($correct_answers);
	}
	public static function idToQuestion($id)
	{

		 $question = \App\Question::where('id',$id)->first();   
		 return $question->question;
	}
	public static function idToPoints($id)
	{

		 $question = \App\Question::where('id',$id)->first();   
		 return $question->points;
	}
	public static function idToAnswer($id)
	{

		 $answer = \App\Answer::where('id',$id)->first();   
		 return $answer->answer;
	}
	public static function idToUser($id)
	{

		 $user = \App\User::where('id',$id)->first();   
		 return $user->name;
	}
}
 ?>