<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $user = \Auth::user();

        
        // Auth gates for: User management
        Gate::define('user_management_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Roles
        Gate::define('role_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('role_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('role_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('role_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('role_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Users
        Gate::define('user_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('user_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('user_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('user_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('user_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Books
        Gate::define('book_access', function ($user) {
            return in_array($user->role_id, [1, 3, 4]);
        });

        // Auth gates for: Books
        Gate::define('book_shelf', function ($user) {
            return in_array($user->role_id, [1, 3, 4]);
        });

        // Auth gates for: Books
        Gate::define('book_access', function ($user) {
            return in_array($user->role_id, [1, 3, 4]);
        });
        Gate::define('book_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('book_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('book_view', function ($user) {
            return in_array($user->role_id, [1, 3, 4]);
        });
        Gate::define('book_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Book codes
        Gate::define('book_code_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('book_code_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('book_code_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('book_code_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('book_code_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Tests
        Gate::define('test_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });

        // Auth gates for: Test
        Gate::define('test_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('test_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('test_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('test_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('test_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Questions
        Gate::define('question_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('question_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('question_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('question_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('question_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Answers
        Gate::define('answer_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('answer_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('answer_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('answer_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('answer_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Videos
        Gate::define('video_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });

        // Auth gates for: Video
        Gate::define('video_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3]);
        });
        Gate::define('video_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('video_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('video_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('video_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: User actions
        Gate::define('user_action_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: General settings
        Gate::define('general_setting_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Districts
        Gate::define('district_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('district_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('district_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('district_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('district_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Schools
        Gate::define('school_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('school_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('school_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('school_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('school_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Book chapters
        Gate::define('book_chapter_access', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('book_chapter_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('book_chapter_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('book_chapter_view', function ($user) {
            return in_array($user->role_id, [1, 2, 3, 4]);
        });
        Gate::define('book_chapter_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

    }
}
