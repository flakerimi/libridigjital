<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Test
 *
 * @package App
 * @property string $name
 * @property text $description
 * @property string $chapter
 * @property string $book
*/
class TestResult extends Model
{
    use SoftDeletes;

    protected $fillable = ['test_id', 'user_id', 'question_id', 'answer_id','correct'];
    
    public static function boot()
    {
        parent::boot();

        TestResult::observe(new \App\Observers\UserActionsObserver);
    }

   
    public function test()
    {
        return $this->belongsTo(Test::class, 'tests');
    }
    public function questions()
    {
        return $this->hasMany(Question::class, 'question_id');
    }
    public function answers()
    {
        return $this->belongsTo(Answer::class, 'answer_id');
    }
    
}
