<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class School
 *
 * @package App
 * @property string $district
 * @property string $name
*/
class School extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'district_id'];
    
    public static function boot()
    {
        parent::boot();

        School::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setDistrictIdAttribute($input)
    {
        $this->attributes['district_id'] = $input ? $input : null;
    }
    
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id')->withTrashed();
    }
    
}
