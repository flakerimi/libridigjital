<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Video
 *
 * @package App
 * @property string $book
 * @property string $name
 * @property text $embed_code
 * @property string $video_file
 * @property string $video_thumbnail
*/
class Video extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'embed_code', 'video_file', 'video_thumbnail', 'book_id'];
    
    public static function boot()
    {
        parent::boot();

        Video::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setBookIdAttribute($input)
    {
        $this->attributes['book_id'] = $input ? $input : null;
    }
    
    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }
    
}
