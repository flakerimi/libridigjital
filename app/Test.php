<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Test
 *
 * @package App
 * @property string $name
 * @property text $description
 * @property string $chapter
 * @property string $book
*/
class Test extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'chapter_id', 'book_id'];
    
    public static function boot()
    {
        parent::boot();

        Test::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setChapterIdAttribute($input)
    {
        $this->attributes['chapter_id'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setBookIdAttribute($input)
    {
        $this->attributes['book_id'] = $input ? $input : null;
    }
    
    public function chapter()
    {
        return $this->belongsTo(BookChapter::class, 'chapter_id')->withTrashed();
    }
    
    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }
    
}
