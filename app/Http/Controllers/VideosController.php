<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreVideosRequest;
use App\Http\Requests\UpdateVideosRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

class VideosController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Video.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('video_access')) {
            return abort(401);
        }
        $videos = Video::all();

        return view('videos.index', compact('videos'));
    }

    /**
     * Show the form for creating new Video.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('video_create')) {
            return abort(401);
        }
        $relations = [
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        return view('videos.create', $relations);
    }

    /**
     * Store a newly created Video in storage.
     *
     * @param  \App\Http\Requests\StoreVideosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideosRequest $request)
    {
        if (! Gate::allows('video_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $video = Video::create($request->all());

        return redirect()->route('videos.index');
    }


    /**
     * Show the form for editing Video.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('video_edit')) {
            return abort(401);
        }
        $relations = [
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        $video = Video::findOrFail($id);

        return view('videos.edit', compact('video') + $relations);
    }

    /**
     * Update Video in storage.
     *
     * @param  \App\Http\Requests\UpdateVideosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVideosRequest $request, $id)
    {
        if (! Gate::allows('video_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $video = Video::findOrFail($id);
        $video->update($request->all());

        return redirect()->route('videos.index');
    }


    /**
     * Display Video.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('video_view')) {
            return abort(401);
        }
        $relations = [
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        $video = Video::findOrFail($id);

        return view('videos.show', compact('video') + $relations);
    }


    /**
     * Remove Video from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('video_delete')) {
            return abort(401);
        }
        $video = Video::findOrFail($id);
        $video->delete();

        return redirect()->route('videos.index');
    }

    /**
     * Delete all selected Video at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('video_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Video::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
