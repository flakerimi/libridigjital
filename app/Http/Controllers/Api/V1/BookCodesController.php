<?php

namespace App\Http\Controllers\Api\V1;

use App\BookCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBookCodesRequest;
use App\Http\Requests\UpdateBookCodesRequest;

class BookCodesController extends Controller
{
    public function index()
    {
        return BookCode::all();
    }

    public function show($id)
    {
        return BookCode::findOrFail($id);
    }

    public function update(UpdateBookCodesRequest $request, $id)
    {
        $book_code = BookCode::findOrFail($id);
        $book_code->update($request->all());

        return $book_code;
    }

    public function store(StoreBookCodesRequest $request)
    {
        $book_code = BookCode::create($request->all());

        return $book_code;
    }

    public function destroy($id)
    {
        $book_code = BookCode::findOrFail($id);
        $book_code->delete();
        return '';
    }
}
