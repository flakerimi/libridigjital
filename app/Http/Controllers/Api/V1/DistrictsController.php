<?php

namespace App\Http\Controllers\Api\V1;

use App\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDistrictsRequest;
use App\Http\Requests\UpdateDistrictsRequest;

class DistrictsController extends Controller
{
    public function index()
    {
        return District::all();
    }

    public function show($id)
    {
        return District::findOrFail($id);
    }

    public function update(UpdateDistrictsRequest $request, $id)
    {
        $district = District::findOrFail($id);
        $district->update($request->all());

        return $district;
    }

    public function store(StoreDistrictsRequest $request)
    {
        $district = District::create($request->all());

        return $district;
    }

    public function destroy($id)
    {
        $district = District::findOrFail($id);
        $district->delete();
        return '';
    }
}
