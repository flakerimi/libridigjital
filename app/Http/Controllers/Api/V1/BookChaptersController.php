<?php

namespace App\Http\Controllers\Api\V1;

use App\BookChapter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBookChaptersRequest;
use App\Http\Requests\UpdateBookChaptersRequest;
use App\Http\Controllers\Traits\FileUploadTrait;

class BookChaptersController extends Controller
{
    use FileUploadTrait;

    public function index()
    {
        return BookChapter::all();
    }

    public function show($id)
    {
        return BookChapter::findOrFail($id);
    }

    public function update(UpdateBookChaptersRequest $request, $id)
    {
        $request = $this->saveFiles($request);
        $book_chapter = BookChapter::findOrFail($id);
        $book_chapter->update($request->all());

        return $book_chapter;
    }

    public function store(StoreBookChaptersRequest $request)
    {
        $request = $this->saveFiles($request);
        $book_chapter = BookChapter::create($request->all());

        return $book_chapter;
    }

    public function destroy($id)
    {
        $book_chapter = BookChapter::findOrFail($id);
        $book_chapter->delete();
        return '';
    }
}
