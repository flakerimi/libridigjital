<?php

namespace App\Http\Controllers\Api\V1;

use App\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSchoolsRequest;
use App\Http\Requests\UpdateSchoolsRequest;

class SchoolsController extends Controller
{
    public function index()
    {
        return School::all();
    }

    public function show($id)
    {
        return School::findOrFail($id);
    }

    public function update(UpdateSchoolsRequest $request, $id)
    {
        $school = School::findOrFail($id);
        $school->update($request->all());

        return $school;
    }

    public function store(StoreSchoolsRequest $request)
    {
        $school = School::create($request->all());

        return $school;
    }

    public function destroy($id)
    {
        $school = School::findOrFail($id);
        $school->delete();
        return '';
    }
}
