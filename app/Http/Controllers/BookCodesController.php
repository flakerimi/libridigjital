<?php

namespace App\Http\Controllers;

use App\BookCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreBookCodesRequest;
use App\Http\Requests\UpdateBookCodesRequest;

class BookCodesController extends Controller
{
    /**
     * Display a listing of BookCode.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('book_code_access')) {
            return abort(401);
        }
        $book_codes = BookCode::all();

        return view('book_codes.index', compact('book_codes'));
    }

    /**
     * Show the form for creating new BookCode.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('book_code_create')) {
            return abort(401);
        }
        $relations = [
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        return view('book_codes.create', $relations);
    }

    /**
     * Store a newly created BookCode in storage.
     *
     * @param  \App\Http\Requests\StoreBookCodesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookCodesRequest $request)
    {
        if (! Gate::allows('book_code_create')) {
            return abort(401);
        }
        $book_code = BookCode::create($request->all());

        return redirect()->route('book_codes.index');
    }


    /**
     * Show the form for editing BookCode.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('book_code_edit')) {
            return abort(401);
        }
        $relations = [
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        $book_code = BookCode::findOrFail($id);

        return view('book_codes.edit', compact('book_code') + $relations);
    }

    /**
     * Update BookCode in storage.
     *
     * @param  \App\Http\Requests\UpdateBookCodesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookCodesRequest $request, $id)
    {
        if (! Gate::allows('book_code_edit')) {
            return abort(401);
        }
        $book_code = BookCode::findOrFail($id);
        $book_code->update($request->all());

        return redirect()->route('book_codes.index');
    }


    /**
     * Display BookCode.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('book_code_view')) {
            return abort(401);
        }
        $relations = [
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        $book_code = BookCode::findOrFail($id);

        return view('book_codes.show', compact('book_code') + $relations);
    }


    /**
     * Remove BookCode from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('book_code_delete')) {
            return abort(401);
        }
        $book_code = BookCode::findOrFail($id);
        $book_code->delete();

        return redirect()->route('book_codes.index');
    }

    /**
     * Delete all selected BookCode at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('book_code_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = BookCode::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
