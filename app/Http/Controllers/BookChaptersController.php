<?php

namespace App\Http\Controllers;

use App\BookChapter;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreBookChaptersRequest;
use App\Http\Requests\UpdateBookChaptersRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

class BookChaptersController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of BookChapter.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('book_chapter_access')) {
            return abort(401);
        }
        $book_chapters = BookChapter::all();

        return view('book_chapters.index', compact('book_chapters'));
    }

    /**
     * Show the form for creating new BookChapter.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('book_chapter_create')) {
            return abort(401);
        }
        $relations = [
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        return view('book_chapters.create', $relations);
    }

    /**
     * Store a newly created BookChapter in storage.
     *
     * @param  \App\Http\Requests\StoreBookChaptersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookChaptersRequest $request)
    {
        if (! Gate::allows('book_chapter_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $book_chapter = BookChapter::create($request->all());

        return redirect()->route('book_chapters.index');
    }


    /**
     * Show the form for editing BookChapter.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('book_chapter_edit')) {
            return abort(401);
        }
        $relations = [
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        $book_chapter = BookChapter::findOrFail($id);

        return view('book_chapters.edit', compact('book_chapter') + $relations);
    }

    /**
     * Update BookChapter in storage.
     *
     * @param  \App\Http\Requests\UpdateBookChaptersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookChaptersRequest $request, $id)
    {
        if (! Gate::allows('book_chapter_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $book_chapter = BookChapter::findOrFail($id);
        $book_chapter->update($request->all());

        return redirect()->route('book_chapters.index');
    }


    /**
     * Display BookChapter.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('book_chapter_view')) {
            return abort(401);
        }
        $relations = [
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
            'tests' => \App\Test::where('chapter_id', $id)->get(),
            'videos' => \App\Video::where('chapter_id', $id)->get(),
        ];

        $book_chapter = BookChapter::findOrFail($id);

        return view('book_chapters.show', compact('book_chapter') + $relations);
    }



    /**
     * Display BookChapter.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function chapter($id)
    {
        if (! Gate::allows('book_chapter_view')) {
            return abort(401);
        }
        $relations = [
             'videos' => \App\Video::where('chapter_id', $id)->get(),
         ];

        $book_chapter = BookChapter::findOrFail($id);

        return view('book_chapters.chapter', compact('book_chapter') + $relations);
    }


    /**
     * Remove BookChapter from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('book_chapter_delete')) {
            return abort(401);
        }
        $book_chapter = BookChapter::findOrFail($id);
        $book_chapter->delete();

        return redirect()->route('book_chapters.index');
    }

    /**
     * Delete all selected BookChapter at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('book_chapter_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = BookChapter::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
