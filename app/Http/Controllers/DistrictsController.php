<?php

namespace App\Http\Controllers;

use App\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreDistrictsRequest;
use App\Http\Requests\UpdateDistrictsRequest;

class DistrictsController extends Controller
{
    /**
     * Display a listing of District.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('district_access')) {
            return abort(401);
        }
        $districts = District::all();

        return view('districts.index', compact('districts'));
    }

    /**
     * Show the form for creating new District.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('district_create')) {
            return abort(401);
        }
        return view('districts.create');
    }

    /**
     * Store a newly created District in storage.
     *
     * @param  \App\Http\Requests\StoreDistrictsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDistrictsRequest $request)
    {
        if (! Gate::allows('district_create')) {
            return abort(401);
        }
        $district = District::create($request->all());

        return redirect()->route('districts.index');
    }


    /**
     * Show the form for editing District.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('district_edit')) {
            return abort(401);
        }
        $district = District::findOrFail($id);

        return view('districts.edit', compact('district'));
    }

    /**
     * Update District in storage.
     *
     * @param  \App\Http\Requests\UpdateDistrictsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDistrictsRequest $request, $id)
    {
        if (! Gate::allows('district_edit')) {
            return abort(401);
        }
        $district = District::findOrFail($id);
        $district->update($request->all());

        return redirect()->route('districts.index');
    }


    /**
     * Display District.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('district_view')) {
            return abort(401);
        }
        $relations = [
            'schools' => \App\School::where('district_id', $id)->get(),
            'users' => \App\User::where('district_id', $id)->get(),
        ];

        $district = District::findOrFail($id);

        return view('districts.show', compact('district') + $relations);
    }


    /**
     * Remove District from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('district_delete')) {
            return abort(401);
        }
        $district = District::findOrFail($id);
        $district->delete();

        return redirect()->route('districts.index');
    }

    /**
     * Delete all selected District at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('district_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = District::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
