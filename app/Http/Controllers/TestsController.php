<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreTestsRequest;
use App\Http\Requests\UpdateTestsRequest;
use Yajra\Datatables\Datatables;

class TestsController extends Controller
{
    /**
     * Display a listing of Test.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('test_access')) {
            return abort(401);
        }
        $tests = Test::all();

        return view('tests.index', compact('tests'));
    }

    /**
     * Show the form for creating new Test.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('test_create')) {
            return abort(401);
        }
        $relations = [
            'chapters' => \App\BookChapter::get()->pluck('chapter_title', 'id')->prepend('Please select', ''),
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        return view('tests.create', $relations);
    }

    /**
     * Store a newly created Test in storage.
     *
     * @param  \App\Http\Requests\StoreTestsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTestsRequest $request)
    {
        if (! Gate::allows('test_create')) {
            return abort(401);
        }
        $test = Test::create($request->all());

        return redirect()->route('tests.index');
    }


    /**
     * Show the form for editing Test.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('test_edit')) {
            return abort(401);
        }
        $relations = [
            'chapters' => \App\BookChapter::get()->pluck('chapter_title', 'id')->prepend('Please select', ''),
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
        ];

        $test = Test::findOrFail($id);

        return view('tests.edit', compact('test') + $relations);
    }

    /**
     * Update Test in storage.
     *
     * @param  \App\Http\Requests\UpdateTestsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTestsRequest $request, $id)
    {
        if (! Gate::allows('test_edit')) {
            return abort(401);
        }
        $test = Test::findOrFail($id);
        $test->update($request->all());

        return redirect()->route('tests.index');
    }


    /**
     * Display Test.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('test_view')) {
            return abort(401);
        }
        $relations = [
            'chapters' => \App\BookChapter::get()->pluck('chapter_title', 'id')->prepend('Please select', ''),
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
            'questions' => \App\Question::where('test_id', $id)->get(),
        ];

        $test = Test::findOrFail($id);

        return view('tests.show', compact('test') + $relations);
    }


  
    /**
     * Take Test.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function take($id)
    {
        if (! Gate::allows('test_view')) {
            return abort(401);
        }
        $relations = [
            'chapters' => \App\BookChapter::get()->pluck('chapter_title', 'id')->prepend('Please select', ''),
            'books' => \App\Book::get()->pluck('name', 'id')->prepend('Please select', ''),
            'questions' => \App\Question::where('test_id', $id)->get(),
        ];

        $test = Test::findOrFail($id);

        return view('tests.take', compact('test') + $relations);
    }


    /**
     * Remove Test from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('test_delete')) {
            return abort(401);
        }
        $test = Test::findOrFail($id);
        $test->delete();

        return redirect()->route('tests.index');
    }

    /**
     * Delete all selected Test at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('test_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Test::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
