<?php

namespace App\Http\Controllers;

use App\Book;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreBooksRequest;
use App\Http\Requests\UpdateBooksRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

class BooksController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Book.
     *
     * @return \Illuminate\Http\Response
     */
    public function shelf()
    {
        if (! Gate::allows('book_shelf')) {
            return abort(401);
        }
        $books = Auth::user()->books()->get()->chunk(6);
 

        return view('books.shelf', compact('books'));
    }

    public function redeem()
    {
        if (! Gate::allows('book_access')) {
            return abort(401);
        }
        $books = Auth::user()->books()->get()->chunk(6);
 

        return view('books.shelf', compact('books'));
    }

    /**
     * Display a listing of Book.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('book_access')) {
            return abort(401);
        }
        $books = Book::all();

        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating new Book.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('book_create')) {
            return abort(401);
        }
        $relations = [
            'teachers' => \App\User::get()->pluck('name', 'id'),
        ];

        return view('books.create', $relations);
    }

    /**
     * Store a newly created Book in storage.
     *
     * @param  \App\Http\Requests\StoreBooksRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBooksRequest $request)
    {
        if (! Gate::allows('book_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $book = Book::create($request->all());
        $book->teachers()->sync(array_filter((array)$request->input('teachers')));

        return redirect()->route('books.index');
    }


    /**
     * Show the form for editing Book.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('book_edit')) {
            return abort(401);
        }
        $relations = [
            'teachers' => \App\User::get()->pluck('name', 'id'),
        ];

        $book = Book::findOrFail($id);

        return view('books.edit', compact('book') + $relations);
    }

    /**
     * Update Book in storage.
     *
     * @param  \App\Http\Requests\UpdateBooksRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBooksRequest $request, $id)
    {
        if (! Gate::allows('book_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $book = Book::findOrFail($id);
        $book->update($request->all());
        $book->teachers()->sync(array_filter((array)$request->input('teachers')));

        return redirect()->route('books.index');
    }


    /**
     * Display Book.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('book_view')) {
            return abort(401);
        }
        $relations = [
            'teachers' => \App\User::get()->pluck('name', 'id'),
            'book_codes' => \App\BookCode::where('book_id', $id)->get(),
            'videos' => \App\Video::where('chapter_id', $id)->get(),
            'book_chapters' => \App\BookChapter::where('book_id', $id)->get(),
            'tests' => \App\Test::where('book_id', $id)->get(),
        ];

        $book = Book::findOrFail($id);

        return view('books.show', compact('book') + $relations);
    }



    /**
     * Display Book.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function book($id)
    {
        if (! Gate::allows('book_view')) {
            return abort(401);
        }
        $relations = [
            'teachers' => \App\User::get()->pluck('name', 'id'),
            'book_codes' => \App\BookCode::where('book_id', $id)->get(),
            'videos' => \App\Video::where('chapter_id', $id)->get(),
            'book_chapters' => \App\BookChapter::where('book_id', $id)->get(),
            'tests' => \App\Test::where('book_id', $id)->get(),
        ];

        $book = Book::findOrFail($id);

        return view('books.book', compact('book') + $relations);
    }


    /**
     * Remove Book from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('book_delete')) {
            return abort(401);
        }
        $book = Book::findOrFail($id);
        $book->delete();

        return redirect()->route('books.index');
    }

    /**
     * Delete all selected Book at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('book_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Book::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
