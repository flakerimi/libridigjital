<?php

namespace App\Http\Controllers;

use Auth;
use App\TestResult;
use App\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreTestsResultRequest;
 use Illuminate\Support\Facades\DB;

class TestResultsController extends Controller
{
    /**
     * Display a listing of Test.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('test_access')) {
            return abort(401);
        }
        $tests = Test::all();

        return view('tests.index', compact('tests'));
    }

    public function view($user_id,$testid)
    {
        echo $testid;
        if (! Gate::allows('test_access')) {
            return abort(401);
        }
        $test_results = TestResult::where('test_id',$testid)->get();
         $test_results_sum =  DB::table('test_results')->where('test_id',$testid)->where('correct',1)->sum('points');
        $nxenesi = \App\Helpers\General::idToUser($user_id);
          return view('test_results.view', compact('test_results','test_results_sum','nxenesi'));
    }


    /**
     * Store a newly created Test in storage.
     *
     * @param  \App\Http\Requests\StoreTestsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function taking(StoreTestsResultRequest $request, $id)
    {
        /*if (! Gate::allows('test_create')) {
            return abort(401);
        }

*/
           $current_user = Auth::user()->id;

          $results = $request->all();
unset($results['_token']);          print_r($results);
         foreach($results as $key => $one) {
            $correct = 0;
            if(strpos($one, '.') !== false)
                $correct = 1;
             $testresult = new TestResult;

                    $testresult->test_id = $id;
                    $testresult->user_id = $current_user;
                    $testresult->question_id = $key;
                    $testresult->answer_id = $one;
                    $testresult->correct = $correct;
                    $testresult->points = \App\Helpers\General::idTopoints($key);

                    $testresult->save();

            }

          
         
        return redirect()->route('books.shelf');

         
     }


     

}
