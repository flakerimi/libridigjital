<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class District
 *
 * @package App
 * @property string $name
 * @property string $code
*/
class District extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'code'];
    
    public static function boot()
    {
        parent::boot();

        District::observe(new \App\Observers\UserActionsObserver);
    }
    
}
