<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Question
 *
 * @package App
 * @property string $test
 * @property string $question
*/
class Question extends Model
{
    use SoftDeletes;

    protected $fillable = ['question', 'test_id','points'];
    
    public static function boot()
    {
        parent::boot();

        Question::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setTestIdAttribute($input)
    {
        $this->attributes['test_id'] = $input ? $input : null;
    }
    
    public function test()
    {
        return $this->belongsTo(Test::class, 'test_id')->withTrashed();
    }
    public function answers()
    {
        return $this->hasMany(Answer::class, 'question_id');
    }
    
}
