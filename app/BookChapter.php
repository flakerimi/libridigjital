<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BookChapter
 *
 * @package App
 * @property string $chapter_title
 * @property string $chapter_file
 * @property string $book
*/
class BookChapter extends Model
{
    use SoftDeletes;

    protected $fillable = ['chapter_title', 'chapter_file', 'book_id'];
    
    public static function boot()
    {
        parent::boot();

        BookChapter::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setBookIdAttribute($input)
    {
        $this->attributes['book_id'] = $input ? $input : null;
    }
    
    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }
    public function vidoes()
    {
        return $this->hasMany(Video::class, 'chapter_id');
    }
    
}
