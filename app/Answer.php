<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Answer
 *
 * @package App
 * @property string $question
 * @property string $answer
 * @property tinyInteger $correct_answer
 * @property integer $points
*/
class Answer extends Model
{
    protected $fillable = ['answer', 'correct_answer', 'question_id'];
    
    public static function boot()
    {
        parent::boot();

        Answer::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setQuestionIdAttribute($input)
    {
        $this->attributes['question_id'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setPointsAttribute($input)
    {
        $this->attributes['points'] = $input ? $input : null;
    }
    
    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id')->withTrashed();
    }
    
}
