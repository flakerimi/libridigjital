<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Book
 *
 * @package App
 * @property string $name
 * @property string $cover
 * @property text $description
 * @property string $type
*/
class Book extends Model
{
    protected $fillable = ['name', 'cover', 'description', 'type'];
    
    public static function boot()
    {
        parent::boot();

        Book::observe(new \App\Observers\UserActionsObserver);
    }
    
    public function teachers()
    {
        return $this->belongsToMany(User::class, 'book_user');
    }
    
    public function students()
    {
        return $this->belongsToMany(User::class, 'book_student');
    }
    public function bookcodes()
    {
        return $this->hasMany(BookCode::class, 'book_codes');
    }
     
}
