<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BookCode
 *
 * @package App
 * @property string $book
 * @property string $code
*/
class BookCode extends Model
{
    protected $fillable = ['code', 'book_id'];
    
    public static function boot()
    {
        parent::boot();

        BookCode::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setBookIdAttribute($input)
    {
        $this->attributes['book_id'] = $input ? $input : null;
    }
    
    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }
    
}
