@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.questions.title')</h3>
    
    {!! Form::model($question, ['method' => 'PUT', 'route' => ['questions.update', $question->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('test_id', 'Test', ['class' => 'control-label']) !!}
                    {!! Form::select('test_id', $tests, old('test_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('test_id'))
                        <p class="help-block">
                            {{ $errors->first('test_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('question', 'Question*', ['class' => 'control-label']) !!}
                    {!! Form::text('question', old('question'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('question'))
                        <p class="help-block">
                            {{ $errors->first('question') }}
                        </p>
                    @endif
                </div>
            </div>
               <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('points', 'Piket*', ['class' => 'control-label']) !!}
                    {!! Form::text('points', old('points'), ['class' => 'form-control', 'placeholder' => 'Piket']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('points'))
                        <p class="help-block">
                            {{ $errors->first('points') }}
                        </p>
                    @endif
                </div>
            </div>   
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

