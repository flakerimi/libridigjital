@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.questions.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.questions.fields.test')</th>
                            <td>{{ $question->test->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.questions.fields.question')</th>
                            <td>{{ $question->question }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#answers" aria-controls="answers" role="tab" data-toggle="tab">Answers</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="answers">
<table class="table table-bordered table-striped {{ count($answers) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.answers.fields.question')</th>
                        <th>@lang('quickadmin.answers.fields.answer')</th>
                        <th>@lang('quickadmin.answers.fields.correct-answer')</th>
                        <th>@lang('quickadmin.answers.fields.points')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($answers) > 0)
            @foreach ($answers as $answer)
                <tr data-entry-id="{{ $answer->id }}">
                    <td>{{ $answer->question->question or '' }}</td>
                                <td>{{ $answer->answer }}</td>
                                <td>{{ Form::checkbox("correct_answer", 1, $answer->correct_answer == 1, ["disabled"]) }}</td>
                                <td>{{ $answer->points }}</td>
                                <td>
                                    @can('answer_view')
                                    <a href="{{ route('answers.show',[$answer->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('answer_edit')
                                    <a href="{{ route('answers.edit',[$answer->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('answer_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['answers.destroy', $answer->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('questions.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop