@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.answers.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.answers.fields.question')</th>
                            <td>{{ $answer->question->question or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.answers.fields.answer')</th>
                            <td>{{ $answer->answer }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.answers.fields.correct-answer')</th>
                            <td>{{ Form::checkbox("correct_answer", 1, $answer->correct_answer == 1, ["disabled"]) }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.answers.fields.points')</th>
                            <td>{{ $answer->points }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('answers.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop