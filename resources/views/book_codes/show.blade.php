@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.book-codes.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.book-codes.fields.book')</th>
                            <td>{{ $book_code->book->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.book-codes.fields.code')</th>
                            <td>{{ $book_code->code }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('book_codes.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop