@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.book-codes.title')</h3>
    @can('book_code_create')
    <p>
        <a href="{{ route('book_codes.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($book_codes) > 0 ? 'datatable' : '' }} @can('book_code_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('book_code_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.book-codes.fields.book')</th>
                        <th>@lang('quickadmin.book-codes.fields.code')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($book_codes) > 0)
                        @foreach ($book_codes as $book_code)
                            <tr data-entry-id="{{ $book_code->id }}">
                                @can('book_code_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $book_code->book->name or '' }}</td>
                                <td>{{ $book_code->code }}</td>
                                <td>
                                    @can('book_code_view')
                                    <a href="{{ route('book_codes.show',[$book_code->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('book_code_edit')
                                    <a href="{{ route('book_codes.edit',[$book_code->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('book_code_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['book_codes.destroy', $book_code->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('book_code_delete')
            window.route_mass_crud_entries_destroy = '{{ route('book_codes.mass_destroy') }}';
        @endcan

    </script>
@endsection