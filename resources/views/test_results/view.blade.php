@extends('layouts.app')
@section('content')
<div class="well">
<h1>Pike: {{$test_results_sum}}; Nxenesi: {{$nxenesi}}</h1>
@if (count($test_results) > 0)
    @foreach ($test_results as $test_result)

<h4>{{	\App\Helpers\General::idToQuestion($test_result->question_id) }}</h4>
@if($test_result->correct == 1) 
<h3 class="bg-success">
{{	\App\Helpers\General::idToAnswer($test_result->answer_id) }} <br>
</h3>
@else
<h3 class="bg-danger">
{{	\App\Helpers\General::idToAnswer($test_result->answer_id) }} <br>
</h3>
@endif
<hr>
@endforeach
@else
    <tr>
        <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
    </tr>
@endif</div>
@endsection