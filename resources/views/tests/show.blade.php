@extends('layouts.app')
@section('content')
<h1 class="page-title"> {{ $test->name }} | {{ $test->book->name or '' }} | {{ $test->chapter->chapter_title or '' }}</h1>



@if (count($questions) > 0)
@foreach ($questions as $question)
<div class="panel panel-default">
    <div class="panel-heading" >



<div class="row">
                        <div class="col col-xs-9">
                            <h3 class="panel-title"" data-toggle="collapse" href="#question_{{ $question->id }}">{{ $question->question }}  <span class="badge"><kbd>pike</kbd>{{$question->points }}</span></h3>
                        </div>
                        <div class="col col-xs-3 text-right">
                            <div class="pull-right">
                                 @can('question_edit')
                <a  id="{{ $question->id }}" data-toggle="modal" data-target="#myModal" class="btn btn-xs btn-info">@lang('quickadmin.qa_create') @lang('quickadmin.answers.title')</a>
            @endcan  
                
             @can('question_edit')
                <a href="{{ route('questions.edit',[$question->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
            @endcan 
            @can('question_delete')
            {!! Form::open(array(
            'style' => 'display: inline-block;',
            'method' => 'DELETE',
            'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
            'route' => ['questions.destroy', $question->id])) !!}
            {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
            {!! Form::close() !!}
            @endcan
                            </div>
                        </div>
                    </div>    </div>



     <ul class="list-group panel-collapse collapse"  id="question_{{ $question->id }}" class="">
      @foreach($question->answers as $answer)
        <li class="list-group-item @if($answer->correct_answer == 1)  list-group-item-success @endif">{{$answer->answer }}</li>
 
        @endforeach 
    </ul>
 
</div>

@endforeach
@else
<tr>
    <td colspan="6">@lang('quickadmin.qa_no_entries_in_table')</td>
</tr>
@endif
<p class="clearfix"></p>
<a href="{{ route('tests.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
 
</div>

<!-- Default bootstrap modal example -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
              {!! Form::open(['method' => 'POST', 'route' => ['answers.store']]) !!}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
                  <div class="row">
                                      {!! Form::hidden('question_id', '', old('question_id'), ['class' => 'form-control select2']) !!}

                <div class="col-xs-12 form-group">
                    {!! Form::label('answer', 'Answer*', ['class' => 'control-label']) !!}
                    {!! Form::text('answer', old('answer'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('answer'))
                        <p class="help-block">
                            {{ $errors->first('answer') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('correct_answer', 'Correct answer', ['class' => 'control-label']) !!}
                    {!! Form::hidden('correct_answer', 0) !!}
                    {!! Form::checkbox('correct_answer', 1, false) !!}
                    <p class="help-block"></p>
                    @if($errors->has('correct_answer'))
                        <p class="help-block">
                            {{ $errors->first('correct_answer') }}
                        </p>
                    @endif
                </div>
            </div>
                  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
      </div>

    
    {!! Form::close() !!}
    </div>
  </div>
</div>
@stop


@section('javascript') 
    <script>
      $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        console.log(link.attr("id"));
        $("input[name=question_id]").val(link.attr("id"));
    });
    </script>
@endsection