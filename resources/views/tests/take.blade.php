@extends('layouts.app')
@section('content')
<h1 class="page-title"> {{ $test->name }} | {{ $test->book->name or '' }} | {{ $test->chapter->chapter_title or '' }}</h1>
@if (count($questions) > 0)
{!! Form::open(['url' => 'tests/taking/' . $test->id ]) !!}
@foreach ($questions as $question)
<div class="panel panel-default tests">
    <div class="panel-heading" >
        <div class="row">
            <div class="col col-xs-12">
                <h3 class="panel-title">{{ $question->question }} <span class="badge pull-right">{{$question->points }}<kbd>pike</kbd></span></h3>
            </div>
            
        </div>
    </div>
    <ul class="list-group funkyradio"  id="question_{{ $question->id }}" class="">
        @foreach($question->answers as $answer)
        <li class="list-group-item ">
            <input type="radio" name="{{ $question->id }}" value="{{$answer->id }}@if($answer->correct_answer == 1).@endif" id="radio-{{ $question->id }}-{{$answer->id }}" />
             <label for="radio-{{ $question->id }}-{{$answer->id }}"> {{$answer->answer }}</label>
        </li>
        @endforeach
    </ul>
    
</div>
@endforeach
{!! Form::submit('Dergo',['class' =>'btn btn-default']); !!}
{!! Form::close() !!}
@else
<div>
    <div colspan="6">@lang('quickadmin.qa_no_entries_in_table')</div>
</div>
@endif
<p class="clearfix"></p>
 </div>
</div>
@stop
@section('javascript')
<script>
$("#myModal").on("show.bs.modal", function(e) {
var link = $(e.relatedTarget);
console.log(link.attr("id"));
$("input[name=question_id]").val(link.attr("id"));
});
</script>
@endsection