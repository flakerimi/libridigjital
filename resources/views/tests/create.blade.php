@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.test.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['tests.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', old('description'), ['class' => 'form-control editor', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('description'))
                        <p class="help-block">
                            {{ $errors->first('description') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('book_id', 'Book', ['class' => 'control-label']) !!}
                    {!! Form::select('book_id', $books, old('book_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('book_id'))
                        <p class="help-block">
                            {{ $errors->first('book_id') }}
                        </p>
                    @endif
                </div>
            </div>            
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('chapter_id', 'Chapter', ['class' => 'control-label']) !!}
                    {!! Form::select('chapter_id', $chapters, old('chapter_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('chapter_id'))
                        <p class="help-block">
                            {{ $errors->first('chapter_id') }}
                        </p>
                    @endif
                </div>
            </div>

            
        </div>
    </div>
 
{!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
    <script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
    <script>

  /*  $("#book_id").select2({
  placeholder: 'Select a number range'
});

$("#chapter_id").select2({
  placeholder: 'Select a number'
});

$("#book_id").on("change", function (e) {
  $("#chapter_id option[value]").remove();
  console.log($(this).val());
  var newOptions = []; // the result of your JSON request

  $("#chapter_id").append(newOptions).val("").trigger("change");
});*/
    $(document).ready(function($){
        $('#book_id').change(function(){
          $.get('/books/' + this.value + '/chapters.json', function(chapters)
          {
              var $chapterSelect = $('#chapter_id');
              // Clear chapterSelect select
              $chapterSelect.find('option').remove().end();
              $(chapters).each(function(id,name) {
                  $chapterSelect.append('<option value="' + name.id + '">' + name.chapter_title + '</option>');
              console.log(name);
              });
          });
        });
    });

 
        $('.editor').each(function () {
                  CKEDITOR.replace($(this).attr('id'),{
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        });
    </script>

@stop