@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.districts.title')</h3>
    @can('district_create')
    <p>
        <a href="{{ route('districts.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($districts) > 0 ? 'datatable' : '' }} @can('district_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('district_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.districts.fields.name')</th>
                        <th>@lang('quickadmin.districts.fields.code')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($districts) > 0)
                        @foreach ($districts as $district)
                            <tr data-entry-id="{{ $district->id }}">
                                @can('district_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $district->name }}</td>
                                <td>{{ $district->code }}</td>
                                <td>
                                    @can('district_view')
                                    <a href="{{ route('districts.show',[$district->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    @endcan
                                    @can('district_edit')
                                    <a href="{{ route('districts.edit',[$district->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                    @endcan
                                    @can('district_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['districts.destroy', $district->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('district_delete')
            window.route_mass_crud_entries_destroy = '{{ route('districts.mass_destroy') }}';
        @endcan

    </script>
@endsection