@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.book-chapters.title')</h3>
    @can('book_chapter_create')
    <p>
        <a href="{{ route('book_chapters.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($book_chapters) > 0 ? 'datatable' : '' }} @can('book_chapter_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('book_chapter_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.book-chapters.fields.chapter-title')</th>
                        <th>@lang('quickadmin.book-chapters.fields.chapter-file')</th>
                        <th>@lang('quickadmin.book-chapters.fields.book')</th>
                        <th>@lang('quickadmin.books.fields.cover')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($book_chapters) > 0)
                        @foreach ($book_chapters as $book_chapter)
                            <tr data-entry-id="{{ $book_chapter->id }}">
                                @can('book_chapter_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $book_chapter->chapter_title }}</td>
                                <td>@if($book_chapter->chapter_file)<a href="{{ asset('uploads/' . $book_chapter->chapter_file) }}" target="_blank">Download file</a>@endif</td>
                                <td>{{ $book_chapter->book->name or '' }}</td>
<td>@if($book_chapter->book && $book_chapter->book->cover)<a href="{{ asset('uploads/' . $book_chapter->book->cover) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $book_chapter->book->cover) }}"/></a>@endif</td>
                                <td>
                                    @can('book_chapter_view')
                                    <a href="{{ route('book_chapters.show',[$book_chapter->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('book_chapter_edit')
                                    <a href="{{ route('book_chapters.edit',[$book_chapter->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('book_chapter_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['book_chapters.destroy', $book_chapter->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('book_chapter_delete')
            window.route_mass_crud_entries_destroy = '{{ route('book_chapters.mass_destroy') }}';
        @endcan

    </script>
@endsection