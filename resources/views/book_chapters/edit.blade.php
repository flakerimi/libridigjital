@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.book-chapters.title')</h3>
    
    {!! Form::model($book_chapter, ['method' => 'PUT', 'route' => ['book_chapters.update', $book_chapter->id], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('chapter_title', 'Chapter title', ['class' => 'control-label']) !!}
                    {!! Form::text('chapter_title', old('chapter_title'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('chapter_title'))
                        <p class="help-block">
                            {{ $errors->first('chapter_title') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('chapter_file', 'Chapter file', ['class' => 'control-label']) !!}
                    @if ($book_chapter->chapter_file)
                        <a href="{{ asset('uploads/' . $book_chapter->chapter_file) }}" target="_blank">Download file</a>
                    @endif
                    {!! Form::file('chapter_file', ['class' => 'form-control']) !!}
                    {!! Form::hidden('chapter_file_max_size', 200) !!}
                    <p class="help-block"></p>
                    @if($errors->has('chapter_file'))
                        <p class="help-block">
                            {{ $errors->first('chapter_file') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('book_id', 'Book', ['class' => 'control-label']) !!}
                    {!! Form::select('book_id', $books, old('book_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('book_id'))
                        <p class="help-block">
                            {{ $errors->first('book_id') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

