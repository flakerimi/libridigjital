@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.video.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['videos.store'], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('book_id', 'Book', ['class' => 'control-label']) !!}
                    {!! Form::select('book_id', $books, old('book_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('book_id'))
                        <p class="help-block">
                            {{ $errors->first('book_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('embed_code', 'Embed code*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('embed_code', old('embed_code'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('embed_code'))
                        <p class="help-block">
                            {{ $errors->first('embed_code') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('video_file', 'Video file', ['class' => 'control-label']) !!}
                    {!! Form::file('video_file', ['class' => 'form-control']) !!}
                    {!! Form::hidden('video_file_max_size', 222) !!}
                    <p class="help-block"></p>
                    @if($errors->has('video_file'))
                        <p class="help-block">
                            {{ $errors->first('video_file') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('video_thumbnail', 'Video thumbnail', ['class' => 'control-label']) !!}
                    {!! Form::file('video_thumbnail', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                    {!! Form::hidden('video_thumbnail_max_size', 100) !!}
                    {!! Form::hidden('video_thumbnail_max_width', 1000) !!}
                    {!! Form::hidden('video_thumbnail_max_height', 1000) !!}
                    <p class="help-block"></p>
                    @if($errors->has('video_thumbnail'))
                        <p class="help-block">
                            {{ $errors->first('video_thumbnail') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

