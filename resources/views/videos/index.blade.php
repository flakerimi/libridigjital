@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.video.title')</h3>
    @can('video_create')
    <p>
        <a href="{{ route('videos.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($videos) > 0 ? 'datatable' : '' }} @can('video_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('video_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.video.fields.book')</th>
                        <th>@lang('quickadmin.video.fields.name')</th>
                        <th>@lang('quickadmin.video.fields.embed-code')</th>
                        <th>@lang('quickadmin.video.fields.video-file')</th>
                        <th>@lang('quickadmin.video.fields.video-thumbnail')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($videos) > 0)
                        @foreach ($videos as $video)
                            <tr data-entry-id="{{ $video->id }}">
                                @can('video_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $video->book->name or '' }}</td>
                                <td>{{ $video->name }}</td>
                                <td>{!! $video->embed_code !!}</td>
                                <td>@if($video->video_file)<a href="{{ asset('uploads/'.$video->video_file) }}" target="_blank">Download file</a>@endif</td>
                                <td>@if($video->video_thumbnail)<a href="{{ asset('uploads/' . $video->video_thumbnail) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $video->video_thumbnail) }}"/></a>@endif</td>
                                <td>
                                    @can('video_view')
                                    <a href="{{ route('videos.show',[$video->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('video_edit')
                                    <a href="{{ route('videos.edit',[$video->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('video_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['videos.destroy', $video->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('video_delete')
            window.route_mass_crud_entries_destroy = '{{ route('videos.mass_destroy') }}';
        @endcan

    </script>
@endsection