@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.video.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.video.fields.book')</th>
                            <td>{{ $video->book->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.video.fields.name')</th>
                            <td>{{ $video->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.video.fields.embed-code')</th>
                            <td>{!! $video->embed_code !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.video.fields.video-file')</th>
                            <td>@if($video->video_file)<a href="{{ asset('uploads/'.$video->video_file) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.video.fields.video-thumbnail')</th>
                            <td>@if($video->video_thumbnail)<a href="{{ asset('uploads/' . $video->video_thumbnail) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $video->video_thumbnail) }}"/></a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('videos.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop