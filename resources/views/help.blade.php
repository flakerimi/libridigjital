

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ndihma</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <style>
        p{
            font-size: 18px;
        }
        #header {
            width:100%;
            height:215px;
            overflow:hidden;
            margin: 0 auto;
            background: url('http://mediaprint.al/images/header-bg.gif') top left repeat-x;

        }
        #logo {
            float:left;
            width: 350px;
            margin-top:40px;
            height:103px;
        }
        #flags {
            float: right;
            height: auto;
            margin: 20px 0 0;
            padding: 0;
            width: auto;
        }
        .gb {background:url('http://mediaprint.al/images/gb.png') no-repeat 0 0 transparent;width:18px;height:12px;border:none;cursor:pointer;}
        .sq {background:url('http://mediaprint.al/images/al.png') no-repeat 0 0 transparent;width:18px;height:12px;border:none;cursor:pointer;}
        .curr {opacity:0.5;}
        #slogan {
            clear: right;
            float: right;
            height: 0px;
            margin-top: 0px;
            padding-right: 0;
            text-align: right;
            width: auto;
        }
        #nav {
            width:100%;
            height:73px;
            float:left;
        }
        #nav ul {
            width:100%;
            overflow:hidden;
            list-style:none;
            padding:0;
        }
        #nav li {
            float:left;
        }
        #nav li:first-child a {
            border-left: 1px solid #CCC;
        }
        #nav li a {
            font-size:16px;
            border-right: 1px solid #CCC;
            border-left: 0px solid;
            line-height:20px;
            display:block;
            text-align:center;
            width:162px;
            text-transform:uppercase;
            text-decoration:none;
            color:#1b1818;
            font-weight:bold;
            padding-top:20px;
            height:73px;
        }
        #nav li a span {
            display:block;
            text-transform:none;
            font-size:0.6875em;
            line-height:1.2307em;
            font-weight:normal;
        }
        #nav li a:hover {
            color:#000;
            background:#bbb;
        }
        #nav .active a {
            color:#fff;
            background:#5dc529;
        }


        #subnav {
            width:340px;
            float:left;
        }
        #subnav ul {
            width:100%;
            overflow:hidden;
        }
        #subnav li {
            float:left;
        }
        #subnav li a {
            color:#474441;
            cursor:pointer;
            display:block;
            font-size:1.5384em;
            font-weight:bold;
            width: 305px;
            height:39px;
            letter-spacing:-1px;
            line-height:1.2307em;
            margin-top:2px;
            padding:14px 0 0 28px;
            text-decoration:none;

            background: url('http://mediaprint.al/images/acc.jpg') no-repeat;
        }
        #subnav li a span {
            display:block;
            text-transform:none;
            font-size:0.6875em;
            line-height:1.2307em;
            font-weight:normal;
        }
        #subnav li a:hover {
            color:#000;
        }
        #subnav li a.active {
            color:#333;
            background: url('http://mediaprint.al/images/acc-h.jpg') no-repeat;
        }




    </style>
</head>
<body>
<div class="container-fluid" id="header">
    <div class="row">
        <div class="container">
            <div id="logo"><a href="http://mediaprint.al/libridigjital"><img src="http://libridigjital.com/assets/img/mp_logo.png" alt="Media Print"></a></div>
            <div id="flags"><ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{Auth::user()->name}} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Profili</a></li>
                            <li><a href="{{ route('auth.change_password') }}">Ndrysho Fjalëkalimin</a></li>
                            <li><a href="#">Rregullime</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#logout" onclick="$('#logout').submit();">
                                    <i class="fa fa-arrow-left"></i>
                                    <span class="title">@lang('quickadmin.logout')</span>
                                </a></li>
                        </ul>
                    </li>
                </ul>

            </div>
            <div id="slogan">Lider në botime dhe imazh</div>
            <div id="nav">
                <ul>
                    <li><a href="/books/shelf">
                            Librat<span>Digjital</span>            </a></li>
                    <li  class="pull-right" ><a href="/ndihma">Përdorimi</a></li>
                </ul>

            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2>Përdorimi</h2>
            <hr/>
        </div>
    </div>

    <div class="row">
        <!-- Capture 1 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/login.JPG" style="width: 500px; height: 180px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Pasi të shtypet <b>http://libridigjital.com</b> do të paraqitet dritarja për identifikim. Ju duhet që të shkruani adresën dhe fjalëkalimin.</p>
            <p>Nëse nuk jeni të regjistruar, atëherë duhet të shtypni butonin <b>Register</b>. Këtu duhet të plotësoni fushat e kërkuara.</p>
            <p>Nëse jeni regjistruar me sukses atëherë mund të përdorni adresën e dhënë dhe fjalëkalimin për të hyrë brenda faqes.</p>
        </div>
        <!-- End Capture 1 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 2 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/ballina.JPG" style="width: 500px; height: 350px" />
        </div>
        <div class="col-md-6 columns">
            <p>Nëse identifikoheni me sukses atëherë do të paraqitet kjo faqe e cila është ballina. Këtu do t’ju ofrohet mirëseardhje dhe disa informacione mbi arsyen dhe funksionimin e platformës.</p>
            <p>Në ballinë do të keni mundësi të hyni te librat. Ju mund të dilni jashtë faqes përmes butonit <b>Nxënës i shkollës</b> pastaj <b>Logout</b>.</p>
        </div>
        <!-- End Capture 2 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 3 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/books.JPG" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Pasi të shtypni te ballina butonin <b>Librat</b> atëherë do të paraqiten librat digjital që i keni zgjedhur.</p>
        </div>
        <!-- End Capture 3 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 4 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/addNewBook.JPG" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Tek faqja e librave digjital ju keni mundësinë që të shtoni libra të tjerë përmes butonit <b>Shto Libër</b>.</p>
            <p>Pasi keni shtypur butonin <b>Shto Libër</b> do të paraqitet dritarja për të shënuar kodin përkatës të librit.</p>
            <p>Nëse ai libër ekziston atëherë ju mund ta shtoni librin në vitrinën tuaj.</p>
        </div>
        <!-- End Capture 4 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 5 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/specificBook.JPG" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Pasi keni zgjedhur një libër ai do të paraqitet si në figurë.</p>
            <p>Secili libër do të ketë: pasqyrën e lëndës, përmbajtjen digjitale, udhëzuesin digjital, konceptin e platformës. Ai gjithashtu do të ketë pjesën e kapitujve të librit dhe atë të testeve.</p>
        </div>
        <!-- End Capture 5 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 5 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/permbledhje.JPG" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Secili libër do të ketë përmbledhje njohurish që mund tu referoheni. Për të thjeshtësuar dhe kuptuar më mirë shpeshherë janë të paraqitura në mënyrë grafike.</p>
        </div>
        <!-- End Capture 5 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 6 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/chapter.JPG" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Për të thjeshtësuar mënyrën e përdorimit të librit digjital, secili libër është i ndarë në kapituj.</p>
            <p>Në secilin kapitull do të mund të gjeni materialin shfletues.</p>
            <p>Gjithashtu në secilin kapitull gjenden videot përkatëse të cilat mund të jenë: shpjegime nga mësuesi i lëndës, shpjegime shtesë me video, skema me video apo dhe ilustrime të ndryshme e grafikë me video.</p>
        </div>
        <!-- End Capture 6 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 7 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/test.JPG" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Nxënësi për çdo kapitull të librit plotëson testin dhe merr vlerësimin përkatës me pikë, (këto pikë do të regjistrohen në databazën e nxënësit dhe nuk ç’bëhen më, pikë të cilat do ketë mundësinë t’i shohë edhe mësuesi).</p>
        </div>
        <!-- End Capture 7 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 9 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/permbajtje.JPG" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Secili libër ka edhe përmbajtjen e librit.</p>
            <p>Pasi keni zgjedhur një libër mund të shihni përmbajtjen e këtij libri përmes butonit <b>Pasqyra e lëndës</b>.</p>
        </div>
        <!-- End Capture 9 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 10 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/udhezues.JPG" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Përmes butonit Udhëzues digjital ju do të keni mundësinë që të merrni udhëzimet e nevojshme mbi përdorimin e platformës dhe informacione shtesë se çfarë ofron libri digjital për mësuesin, nxënësin dhe çfarë mund të realizoni me përdorimin e tij.</p>
        </div>
        <!-- End Capture 10 -->
    </div>
    <hr />



</div>
<div class="col-md-2">
</div>
</div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
