@extends('layouts.app')

@section('content')
<div class="page-header">
  <h1>Librat digjital<small><a href="" style="float: right;"  data-toggle="modal" data-target="#myModal" class="btn btn-outline">Shto Liber</a></small></h1>
</div>
    
<div class="container">
    <div class="row shelfing">    
    @if (count($books) > 0)
        @foreach ($books as $items)
             @foreach($items as $item)
            <div class="col-xs-4 col-md-2">
                <a href="/books/shelf/{{$item->id}}" class=" ">
                    <img src="{{ asset('/uploads/' . $item->cover) }}"  class="img-responsive book" /> 
                </a>   
            </div>
            @endforeach
            <div class="col-xs-12 shelf ">s</div>
                                                                  
         @endforeach

     @endif 
    </div>
</div>  
 
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        {!! Form::open(['method' => 'POST', 'route' => ['books.redeem'], 'files' => false,]) !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Shto libër</h4>
      </div>
      <div class="modal-body">
          <div class="row">

        
                <div class="col-xs-12 form-group">
                    {!! Form::label('code', 'Shkruaj kodin që gjendet mbrapa librit', ['class' => 'control-label']) !!}
                    <div class="input-group">
                    {!! Form::text('code', old('code'), ['class' => 'form-control', 'placeholder' => '']) !!}
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Gjej!</button>
                    </span>
                    </div>
                    <p class="help-block"></p>
                    @if($errors->has('code'))
                        <p class="help-block">
                            {{ $errors->first('code') }}
                        </p>
                    @endif
                </div>
            </div>
              <div class="row hidden">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div> 

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Mbyll</button>
        <button type="button" class="btn btn-primary">Shto</button>
      </div>
        {!! Form::close() !!}
    </div>
  </div>
</div>       
@stop
