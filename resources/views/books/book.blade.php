@extends('layouts.app')
@section('content')
 <div class="row book">
    <div class="col-md-3">  <h1>{{ $book->name }}</h1>@if($book->cover)<a href="{{ asset('uploads/' . $book->cover) }}" target="_blank"><img width="100%" src="{{ asset('uploads/' . $book->cover) }}"/></a>@endif <br> {!!$book->description!!} </div>
    <div class="col-md-3">
      <h1> Njesitë</h1>
        @if (count($book_chapters) > 0)
        @foreach ($book_chapters as $book_chapter)
        <div class="media" data-entry-id="{{ $book_chapter->id }}">
           
            <div class="media-body">
               
              <a  href="/book_chapters/chapter/{{ $book_chapter->id }}?file=/uploads/{{ $book_chapter->chapter_file }}" class="btn btn-primary btn-lg test-buttons "  >
                 <h4 class="media-heading">{{ $book_chapter->chapter_title }}</h4>
                </a>
            </div>
        </div>
        @endforeach
        @else
        <div>
            <div colspan="7">@lang('quickadmin.qa_no_entries_in_table')</div>
        </div>
        @endif
    </div>
    <div class="col-md-3">  
    <h1>Testet</h1>

    @if (count($tests) > 0)
        @foreach ($tests as $test)


        <div class="media"  data-entry-id="{{ $test->id }}">
               @php $TestResult = \App\TestResult::where([['test_id','=',$test->id],['user_id','=', Auth::user()->id]])->first();  
                 @endphp
                @if($TestResult == null)
                @can('test_view')
                <a href="/tests/take/{{$test->id}}" class="btn test-buttons  btn-xs btn-primary"> <h4 class="media-heading">{{ $test->name }}</h4></a>
                @endcan
                @else 
                   <a href="#" class="btn test-buttons  btn-xs btn-primary"><h4 class="media-heading"> Keni bere testin tashme.</h4></a>
                @endif
           
        </div>
        @endforeach
        @else
        <div>
            <div colspan="8">@lang('quickadmin.qa_no_entries_in_table')</div>
        </div>
    @endif</div>
   
</div> 

</div>
            @stop