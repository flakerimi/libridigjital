@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.books.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.books.fields.name')</th>
                            <td>{{ $book->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.books.fields.cover')</th>
                            <td>@if($book->cover)<a href="{{ asset('uploads/' . $book->cover) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $book->cover) }}"/></a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.books.fields.description')</th>
                            <td>{!! $book->description !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.books.fields.type')</th>
                            <td>{{ $book->type }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.books.fields.teachers')</th>
                            <td>
                                @foreach ($book->teachers as $singleTeachers)
                                    <span class="label label-info label-many">{{ $singleTeachers->name }}</span>
                                @endforeach
                            </td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#bookcodes" aria-controls="bookcodes" role="tab" data-toggle="tab">Book codes</a></li>
<li role="presentation" class=""><a href="#video" aria-controls="video" role="tab" data-toggle="tab">Video </a></li>
<li role="presentation" class=""><a href="#bookchapters" aria-controls="bookchapters" role="tab" data-toggle="tab">Book chapters</a></li>
<li role="presentation" class=""><a href="#test" aria-controls="test" role="tab" data-toggle="tab">Test</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="bookcodes">
<table class="table table-bordered table-striped {{ count($book_codes) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.book-codes.fields.book')</th>
                        <th>@lang('quickadmin.book-codes.fields.code')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($book_codes) > 0)
            @foreach ($book_codes as $book_code)
                <tr data-entry-id="{{ $book_code->id }}">
                    <td>{{ $book_code->book->name or '' }}</td>
                                <td>{{ $book_code->code }}</td>
                                <td>
                                    @can('book_code_view')
                                    <a href="{{ route('book_codes.show',[$book_code->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('book_code_edit')
                                    <a href="{{ route('book_codes.edit',[$book_code->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('book_code_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['book_codes.destroy', $book_code->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="6">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="video">
<table class="table table-bordered table-striped {{ count($videos) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.video.fields.book')</th>
                        <th>@lang('quickadmin.video.fields.name')</th>
                        <th>@lang('quickadmin.video.fields.embed-code')</th>
                        <th>@lang('quickadmin.video.fields.video-file')</th>
                        <th>@lang('quickadmin.video.fields.video-thumbnail')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($videos) > 0)
            @foreach ($videos as $video)
                <tr data-entry-id="{{ $video->id }}">
                    <td>{{ $video->book->name or '' }}</td>
                                <td>{{ $video->name }}</td>
                                <td>{!! $video->embed_code !!}</td>
                                <td>@if($video->video_file)<a href="{{ asset('uploads/' . $video->video_file) }}" target="_blank">Download file</a>@endif</td>
                                <td>@if($video->video_thumbnail)<a href="{{ asset('uploads/' . $video->video_thumbnail) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $video->video_thumbnail) }}"/></a>@endif</td>
                                <td>
                                    @can('video_view')
                                    <a href="{{ route('videos.show',[$video->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('video_edit')
                                    <a href="{{ route('videos.edit',[$video->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('video_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['videos.destroy', $video->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="bookchapters">
<table class="table table-bordered table-striped {{ count($book_chapters) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.book-chapters.fields.chapter-title')</th>
                        <th>@lang('quickadmin.book-chapters.fields.chapter-file')</th>
                        <th>@lang('quickadmin.book-chapters.fields.book')</th>
                        <th>@lang('quickadmin.books.fields.cover')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($book_chapters) > 0)
            @foreach ($book_chapters as $book_chapter)
                <tr data-entry-id="{{ $book_chapter->id }}">
                    <td>{{ $book_chapter->chapter_title }}</td>
                                <td>@if($book_chapter->chapter_file)<a href="{{ asset('uploads/' . $book_chapter->chapter_file) }}" target="_blank">Download file</a>@endif</td>
                                <td>{{ $book_chapter->book->name or '' }}</td>
<td>@if($book_chapter->book && $book_chapter->book->cover)<a href="{{ asset('uploads/' . $book_chapter->book->cover) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $book_chapter->book->cover) }}"/></a>@endif</td>
                                <td>
                                    @can('book_chapter_view')
                                    <a href="{{ route('book_chapters.show',[$book_chapter->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('book_chapter_edit')
                                    <a href="{{ route('book_chapters.edit',[$book_chapter->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('book_chapter_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['book_chapters.destroy', $book_chapter->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="test">
<table class="table table-bordered table-striped {{ count($tests) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.test.fields.name')</th>
                        <th>@lang('quickadmin.test.fields.description')</th>
                        <th>@lang('quickadmin.test.fields.chapter')</th>
                        <th>@lang('quickadmin.test.fields.book')</th>
                        <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>
        @if (count($tests) > 0)
            @foreach ($tests as $test)
                <tr data-entry-id="{{ $test->id }}">
                    <td>{{ $test->name }}</td>
                                <td>{!! $test->description !!}</td>
                                <td>{{ $test->chapter->chapter_title or '' }}</td>
                                <td>{{ $test->book->name or '' }}</td>
                                <td>
                                    @can('test_view')
                                    <a href="{{ route('tests.show',[$test->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('test_edit')
                                    <a href="{{ route('tests.edit',[$test->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('test_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['tests.destroy', $test->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('books.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop