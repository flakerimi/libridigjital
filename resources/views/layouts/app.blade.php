@if(Auth::user()->role_id == 1)
    @include('layouts.admin')
@elseif(Auth::user()->role_id == 3)
    @include('layouts.teacher')
@elseif(Auth::user()->role_id == 4)
    @include('layouts.student')
@else
    @include('layouts.admin')
@endif
