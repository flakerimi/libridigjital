

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ndihma</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style>
        p{
            font-size: 18px;
        }
        /* @media (max-width: 768px) {
       p{font-size: 10px;}
   }

   @media (max-width: @screen-sm) {
    p{font-size: 14px;}*/
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2>Ndihma</h2>
            <hr/>
        </div>
    </div>

    <div class="row">
        <!-- Capture 1 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/mp_logo.png" style="width: 500px; height: 180px"/>
        </div>
       
        <!-- End Capture 1 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 2 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="http://libridigjital.com/assets/img/img/ballina.jpg" style="width: 500px; height: 350px" />
        </div>
        <div class="col-md-6 columns">
            <p>Nëse identifikoheni me sukses atëherë do të paraqitet kjo faqe e cila është ballina ku do të ju ofrohet mirëseardhje gjithashtu edhe disa informacione mbi arsyen dhe funksionimin e platformës.</p>
            <p>Në ballinë do të keni mundësi që të hyni tek librat, gjithashtu mund që të dilni jashtë faqës përmes butonit <b>Nxënës i shkollës</b> pastaj <b>Logout</b>.</p>
        </div>
        <!-- End Capture 2 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 3 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="img/books.jpg" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Pasi që të shtypni tek ballina butonin <b>Librat</b> atëherë do të parqiten librat digjital që i keni zgjedhur.</p>
        </div>
        <!-- End Capture 3 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 4 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="img/addNewBook.jpg" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Tek faqja e librave digjital ju keni mundësinë që të shtoni libra të tjerë përmes butonit <b>Shto Libër</b></p>
            <p>Pasi që keni shtypur butonin <b>Shto Libër</b> do të paraqitet dritarja për të shënuar kodin përkatës të librit.</p>
            <p>Nëse ai libër egziston atëherë ju mund ta shtoni librin në vitrinën tuaj</p>
        </div>
        <!-- End Capture 4 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 5 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="img/specificBook.jpg" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Pasi që ju keni zgjedhur një libër ai do të paraqitet si në këtë figurë.</p>
            <p>Secili libër do të ketë: Pasqyren e lëndës, Përmbajtjen digjitale, Udhëzuesin digjital, Konceptin e platformës, por gjithashtu do të ketë pjesën e kapitujve të librit dhe atë të testeve</p>
        </div>
        <!-- End Capture 5 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 5 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="img/permbledhje.jpg" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Secili libër do të ketë përmbledhje njohurish ku secili lexues mund që t'iu referohet për të parë atë, ku për të thjeshtësuar dhe kuptuar më mirë shpeshherë janë të paraqitur në mënyre grafike.</p>
        </div>
        <!-- End Capture 5 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 6 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="img/chapter.jpg" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Për të thjeshtësuar mënyrën e përdorimit të librit digjital, secili libër është i ndarë në kapituj.</p>
            <p>Në secilin kapitull do të mund të gjeni materialin shfletues.</p>
            <p>Gjithashtu në secilin kapitull do të jenë videot përkatëse të cilat mund të jenë: shpjegime nga mësuesi i lëndës, shpjegime shtesë me video, skema me video apo dhe ilustrime të ndryshme, e grafikë me video.</p>
        </div>
        <!-- End Capture 6 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 7 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="img/test.jpg" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Nxënësi për çdo kapitull të librit plotëson testin, dhe merr vlerësimin përkatës me pikë, (këto pikë do të regjistrohen në databazën e nxënësit dhe nuk ç’bëhen më, pikë të cilat do ketë mundësinë t’i shohë dhe mësuesi).</p>
        </div>
        <!-- End Capture 7 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 9 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="img/permbajtje.jpg" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Secili libër do të ketë edhe përmbajtjen e librit.</p>
            <p>Secili nga lexuesit pasi që të ketë zgjedhur një libër mund të shohë përmbajtjen e atij libri përmes butonit <b>Pasqyra e lëndës</b></p>
        </div>
        <!-- End Capture 9 -->
    </div>
    <hr />

    <div class="row">
        <!-- Capture 10 -->
        <div class="col-md-6">
            <img alt="about-us-page" src="img/udhezues.jpg" style="width: 500px; height: 350px"/>
        </div>
        <div class="col-md-6 columns">
            <p>Përmes butonit <b>Udhëzues digjital</b> ju do të keni mundësinë që të merrni udhëzimet e nevojshme mbi përdorimin e platformës dhe informacione shtesë se çfare ofron libri digjital për mësuesin, nxënësin dhe çfarë mund të realizoni me përdorimin e tij.</p>
        </div>
        <!-- End Capture 10 -->
    </div>
    <hr />



</div>
<div class="col-md-2">
</div>
</div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
