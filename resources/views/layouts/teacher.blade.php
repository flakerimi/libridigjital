<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,700">
    <link rel="stylesheet" href="/assets/css/styles.min.css">
     @yield('styles')

 </head>

<body>
    <div class="header-blue">
        <div class="container">
            @include('partials.topnav')  
        </div>
        <div class="container">
            @yield('content')
                    
        </div>
           
    </div>   
   

   
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
 @yield('scripts')
</body>
</html>