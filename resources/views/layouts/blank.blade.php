     @yield('styles')
 
    <div class="container">
        @yield('content')
    </div>
      @yield('scripts')
