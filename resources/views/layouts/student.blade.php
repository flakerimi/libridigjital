<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,700">
    <link rel="stylesheet" href="/assets/css/styles.min.css">
     @yield('styles')
<style>
#header {
	width:100%;
	height:215px;
	overflow:hidden;
	margin: 0 auto;
	background: url('http://mediaprint.al/images/header-bg.gif') top left repeat-x;

}
#logo {
	float:left;
	width: 350px;
	margin-top:40px;
	height:103px;
}
#flags {
  float: right;
  height: auto;
  margin: 20px 0 0;
  padding: 0;
  width: auto;
}
.gb {background:url('http://mediaprint.al/images/gb.png') no-repeat 0 0 transparent;width:18px;height:12px;border:none;cursor:pointer;}
.sq {background:url('http://mediaprint.al/images/al.png') no-repeat 0 0 transparent;width:18px;height:12px;border:none;cursor:pointer;}
.curr {opacity:0.5;}
#slogan {
  clear: right;
  float: right;
  height: 0px;
  margin-top: 0px;
  padding-right: 0;
  text-align: right;
  width: auto;
}
#nav {
	width:100%;
	height:73px;
	float:left;
}
#nav ul {
	width:100%;
	overflow:hidden;
list-style:none;
padding:0;
}
#nav li {
	float:left;
}
#nav li:first-child a {
	border-left: 1px solid #CCC;
}
#nav li a {
	font-size:16px;
	border-right: 1px solid #CCC;
	border-left: 0px solid;
	line-height:20px;
	display:block;
	text-align:center;
	width:162px;
	text-transform:uppercase;
	text-decoration:none;
	color:#1b1818;
	font-weight:bold;
	padding-top:20px;
	height:73px;
}
#nav li a span {
	display:block;
	text-transform:none;
	font-size:0.6875em;
	line-height:1.2307em;
	font-weight:normal;
}
#nav li a:hover {
	color:#000;
	background:#bbb;
}
#nav .active a {
	color:#fff;
	background:#5dc529;
}


#subnav {
	width:340px;
	float:left;
}
#subnav ul {
	width:100%;
	overflow:hidden;
}
#subnav li {
	float:left;
}
#subnav li a {
	color:#474441;
	cursor:pointer;
	display:block;
	font-size:1.5384em;
	font-weight:bold;
	width: 305px;
	height:39px;
	letter-spacing:-1px;
	line-height:1.2307em;
	margin-top:2px;
	padding:14px 0 0 28px;
	text-decoration:none;

	background: url('http://mediaprint.al/images/acc.jpg') no-repeat;
}
#subnav li a span {
	display:block;
	text-transform:none;
	font-size:0.6875em;
	line-height:1.2307em;
	font-weight:normal;
}
#subnav li a:hover {
	color:#000;
}
#subnav li a.active {
	color:#333;
	background: url('http://mediaprint.al/images/acc-h.jpg') no-repeat;
}

</style>
 </head>

<body>
<div class="container-fluid" id="header">
<div class="row">
	<div class="container">
      <div id="logo"><a href="http://mediaprint.al/libridigjital"><img src="http://libridigjital.com/assets/img/mp_logo.png" alt="Media Print"></a></div>
		<div id="flags"><ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{Auth::user()->name}} <span class="caret"></span></a>
          <ul class="dropdown-menu">
              <li><a href="#">Profili</a></li>
              <li><a href="{{ route('auth.change_password') }}">Ndrysho Fjalëkalimin</a></li>
              <li><a href="#">Rregullime</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.logout')</span>
                </a></li>
          </ul>
        </li>
      </ul>

 			  		</div>
					<div id="slogan">Lider në botime dhe imazh</div>
		      <div id="nav">
        <ul>
                               <li><a href="/books/shelf">
            Librat<span>Digjital</span>            </a></li>
                <li  class="pull-right" ><a href="/ndihma">Ndihma</a></li>
                  </ul>

      </div>
    </div>
      </div>
    </div>    <div class="header-blue">
        <div class="container-fluid">
           
        </div>
        <div class="container">
            @yield('content')
                    
        </div>
           
    </div>   
   

   
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
 @yield('scripts')
 
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('quickadmin.logout')</button>
{!! Form::close() !!}
</body>
</html>