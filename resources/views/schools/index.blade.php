@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.schools.title')</h3>
    @can('school_create')
    <p>
        <a href="{{ route('schools.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>
    @endcan

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($schools) > 0 ? 'datatable' : '' }} @can('school_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('school_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.schools.fields.district')</th>
                        <th>@lang('quickadmin.schools.fields.name')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($schools) > 0)
                        @foreach ($schools as $school)
                            <tr data-entry-id="{{ $school->id }}">
                                @can('school_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $school->district->name or '' }}</td>
                                <td>{{ $school->name }}</td>
                                <td>
                                    @can('school_view')
                                    <a href="{{ route('schools.show',[$school->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    @endcan
                                    @can('school_edit')
                                    <a href="{{ route('schools.edit',[$school->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                    @endcan
                                    @can('school_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['schools.destroy', $school->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('school_delete')
            window.route_mass_crud_entries_destroy = '{{ route('schools.mass_destroy') }}';
        @endcan

    </script>
@endsection