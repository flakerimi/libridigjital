@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 text-white">
           <h2>Mir&euml; se vini n&euml; platform&euml;n - Libri digjital</h2>

<p>Duke iu p&euml;rgjigjur ndryshimeve t&euml; shoq&euml;ris&euml; dhe k&euml;rkesave t&euml; kurrikul&euml;s b&euml;rtham&euml; t&euml; arsimit parauniversitar ju ofrojm&euml; integrimin n&euml; epok&euml;n digjitale.</p>

<p>Nx&euml;n&euml;sve jo vet&euml;m q&euml; u nevojiten kompetenca digjitale, por u duhet edhe t&euml; arrijn&euml; shkall&euml;n e alfabetizmit t&euml; plot&euml; funksional &nbsp;n&euml; m&euml;nyr&euml; q&euml; t&euml; integrohen &nbsp;n&euml; ekonomin&euml; bot&euml;rore digjitale.</p>

<p>Duke besuar n&euml; fuqin&euml; kryesore t&euml; bot&euml;s digjitale synojm&euml; q&euml; p&euml;rmes librit digjital t&rsquo;u japim lexuesve-p&euml;rdorues t&euml; saj njohurit&euml; dhe shkatht&euml;sit&euml; e duhura p&euml;r t&euml; qen&euml; qytetar&euml; t&euml; aft&euml; t&euml; epok&euml;s digjitale n&euml; t&euml; cil&euml;n jetojm&euml;:</p>

<p>
	<br>
</p>

<p><strong>P&euml;rmbajtja digjitale e teksteve shkollore:</strong></p>

<p style="margin-left: 20px;">&Euml;sht&euml; n&euml; p&euml;rputhje t&euml; plot&euml; me standardet e teksteve shkollore t&euml; miratuara nga MAS-i;</p>

<p style="margin-left: 20px;">Integron m&euml;simdh&euml;nien dhe m&euml;simnx&euml;nien interaktive t&euml; l&euml;nd&euml;ve;</p>

<p style="margin-left: 20px;">Integron interaktivitetin midis nx&euml;n&euml;sve dhe m&euml;suesve;</p>

<p style="margin-left: 20px;">P&euml;rdoret n&euml; grup ose individualisht;</p>

<p style="margin-left: 20px;">P&euml;rdoret n&euml; shkoll&euml; ose n&euml; distanc&euml;;</p>

<p style="margin-left: 20px;">Funksionon 100% me tabletat digjitale t&euml; shkollave tona;</p>

<p style="margin-left: 20px;">&Euml;sht&euml; n&euml; dispozicion t&euml; p&euml;rdoruesve pa nd&euml;rprerje;</p>

<p style="margin-left: 20px;">P&euml;rdit&euml;sohet nga ana teknike dhe p&euml;rmbajt&euml;sore, pa cenuar funksionimin e saj;</p>

<p style="margin-left: 20px;">&Euml;sht&euml; cil&euml;sisht e aksesueshme dhe siguron cil&euml;sin&euml; e m&euml;simdh&euml;nies dhe m&euml;simnx&euml;nies.</p>
        </div>
    </div>
@endsection
