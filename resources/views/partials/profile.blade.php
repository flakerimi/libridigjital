@inject('request', 'Illuminate\Http\Request')
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     </div>

         <div class="collapse navbar-collapse" id="#nav">
      <ul class="nav navbar-nav">
      
                
                @can('book_access')
                <li class="{{ $request->segment(1) == 'books' ? 'active active-sub' : '' }}">
                        <a href="{{ route('books.shelf') }}">
                            <i class="fa fa-book"></i>
                            <span class="title">
                               LIBRAT
                            </span>
                        </a>
                        
                    </li>
                @endcan
           
           
              
              
            
           
           
         
          
        </ul>


      <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{Auth::user()->name}} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Profili</a></li>
            <li><a href="#">Rregullime</a></li>
             <li role="separator" class="divider"></li>
            <li><a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.logout')</span>
                </a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

      
 
 
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('quickadmin.logout')</button>
{!! Form::close() !!}