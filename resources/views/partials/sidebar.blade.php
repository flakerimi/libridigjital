@inject('request', 'Illuminate\Http\Request')
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu"
            data-keep-expanded="false"
            data-auto-scroll="true"
            data-slide-speed="200">
            
            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('quickadmin.qa_dashboard')</span>
                </a>
            </li>

            
            @can('user_management_access')
            <li class="">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span class="title">@lang('quickadmin.user-management.title')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                
                @can('role_access')
                <li class="{{ $request->segment(1) == 'roles' ? 'active active-sub' : '' }}">
                        <a href="{{ route('roles.index') }}">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                @lang('quickadmin.roles.title')
                            </span>
                        </a>
                    </li>
                @endcan
                @can('user_access')
                <li class="{{ $request->segment(1) == 'users' ? 'active active-sub' : '' }}">
                        <a href="{{ route('users.index') }}">
                            <i class="fa fa-user"></i>
                            <span class="title">
                                @lang('quickadmin.users.title')
                            </span>
                        </a>
                    </li>
                @endcan
                @can('user_action_access')
                <li class="{{ $request->segment(1) == 'user_actions' ? 'active active-sub' : '' }}">
                        <a href="{{ route('user_actions.index') }}">
                            <i class="fa fa-th-list"></i>
                            <span class="title">
                                @lang('quickadmin.user-actions.title')
                            </span>
                        </a>
                    </li>
                @endcan
                </ul>
            </li>
            @endcan
            @can('book_access')
            <li class="">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span class="title">@lang('quickadmin.books.title')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                
                @can('book_access')
                <li class="{{ $request->segment(1) == 'books' ? 'active active-sub' : '' }}">
                        <a href="{{ route('books.index') }}">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                @lang('quickadmin.books.title')
                            </span>
                        </a>
                    </li>
                @endcan
                @can('book_code_access')
                <li class="{{ $request->segment(1) == 'book_codes' ? 'active active-sub' : '' }}">
                        <a href="{{ route('book_codes.index') }}">
                            <i class="fa fa-barcode"></i>
                            <span class="title">
                                @lang('quickadmin.book-codes.title')
                            </span>
                        </a>
                    </li>
                @endcan
                @can('book_chapter_access')
                <li class="{{ $request->segment(1) == 'book_chapters' ? 'active active-sub' : '' }}">
                        <a href="{{ route('book_chapters.index') }}">
                            <i class="fa fa-file-text-o"></i>
                            <span class="title">
                                @lang('quickadmin.book-chapters.title')
                            </span>
                        </a>
                    </li>
                @endcan
                </ul>
            </li>
            @endcan
            @can('test_access')
            <li class="">
                <a href="#">
                    <i class="fa fa-server"></i>
                    <span class="title">@lang('quickadmin.tests.title')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                
                @can('test_access')
                <li class="{{ $request->segment(1) == 'tests' ? 'active active-sub' : '' }}">
                        <a href="{{ route('tests.index') }}">
                            <i class="fa fa-list-ol"></i>
                            <span class="title">
                                @lang('quickadmin.test.title')
                            </span>
                        </a>
                    </li>
                @endcan
                @can('question_access')
                <li class="{{ $request->segment(1) == 'questions' ? 'active active-sub' : '' }}">
                        <a href="{{ route('questions.index') }}">
                            <i class="fa fa-question"></i>
                            <span class="title">
                                @lang('quickadmin.questions.title')
                            </span>
                        </a>
                    </li>
                @endcan
                @can('answer_access')
                <li class="{{ $request->segment(1) == 'answers' ? 'active active-sub' : '' }}">
                        <a href="{{ route('answers.index') }}">
                            <i class="fa fa-check-square-o"></i>
                            <span class="title">
                                @lang('quickadmin.answers.title')
                            </span>
                        </a>
                    </li>
                @endcan
                </ul>
            </li>
            @endcan
            @can('video_access')
            <li class="">
                <a href="#">
                    <i class="fa fa-youtube-play"></i>
                    <span class="title">@lang('quickadmin.videos.title')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                
                @can('video_access')
                <li class="{{ $request->segment(1) == 'videos' ? 'active active-sub' : '' }}">
                        <a href="{{ route('videos.index') }}">
                            <i class="fa fa-video-camera"></i>
                            <span class="title">
                                @lang('quickadmin.video.title')
                            </span>
                        </a>
                    </li>
                @endcan
                </ul>
            </li>
            @endcan
            @can('general_setting_access')
            <li class="">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span class="title">@lang('quickadmin.general-settings.title')</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                
                @can('district_access')
                <li class="{{ $request->segment(1) == 'districts' ? 'active active-sub' : '' }}">
                        <a href="{{ route('districts.index') }}">
                            <i class="fa fa-gears"></i>
                            <span class="title">
                                @lang('quickadmin.districts.title')
                            </span>
                        </a>
                    </li>
                @endcan
                @can('school_access')
                <li class="{{ $request->segment(1) == 'schools' ? 'active active-sub' : '' }}">
                        <a href="{{ route('schools.index') }}">
                            <i class="fa fa-gears"></i>
                            <span class="title">
                                @lang('quickadmin.schools.title')
                            </span>
                        </a>
                    </li>
                @endcan
                </ul>
            </li>
            @endcan

            

            
            @php ($unread = App\MessengerTopic::countUnread())
            <li class="{{ $request->segment(1) == 'messenger' ? 'active' : '' }} {{ ($unread > 0 ? 'unread' : '') }}">
                <a href="{{ route('messenger.index') }}">
                    <i class="fa fa-envelope"></i>

                    <span>Messages</span>
                    @if($unread > 0)
                        {{ ($unread > 0 ? '('.$unread.')' : '') }}
                    @endif
                </a>
            </li>
            <style>
                .page-sidebar-menu .unread * {
                    font-weight:bold !important;
                }
            </style>

            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key"></i>
                    <span class="title">Change password</span>
                </a>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.qa_logout')</span>
                </a>
            </li>
        </ul>
    </div>
</div>
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('quickadmin.logout')</button>
{!! Form::close() !!}
