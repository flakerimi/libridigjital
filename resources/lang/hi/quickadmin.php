<?php

return [
		'user-management' => [		'title' => 'User Management',		'created_at' => 'Time',		'fields' => [		],	],
		'roles' => [		'title' => 'Roles',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',		],	],
		'users' => [		'title' => 'Users',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'email' => 'Email',			'password' => 'Password',			'role' => 'Role',			'remember-token' => 'Remember token',			'district' => 'District',			'school' => 'School',			'approved' => 'Approved',		],	],
		'books' => [		'title' => 'Books Module',		'created_at' => 'Time',		'fields' => [		],	],
		'books' => [		'title' => 'Books',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'cover' => 'Cover',			'description' => 'Description',			'type' => 'Type',			'teachers' => 'Teachers',		],	],
		'book-codes' => [		'title' => 'Book codes',		'created_at' => 'Time',		'fields' => [			'book' => 'Book',			'code' => 'Code',		],	],
		'tests' => [		'title' => 'Tests',		'created_at' => 'Time',		'fields' => [		],	],
		'test' => [		'title' => 'Test',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'description' => 'Description',			'chapter' => 'Chapter',			'book' => 'Book',		],	],
		'questions' => [		'title' => 'Questions',		'created_at' => 'Time',		'fields' => [			'test' => 'Test',			'question' => 'Question',		],	],
		'answers' => [		'title' => 'Answers',		'created_at' => 'Time',		'fields' => [			'question' => 'Question',			'answer' => 'Answer',			'correct-answer' => 'Correct answer',			'points' => 'Points',		],	],
		'videos' => [		'title' => 'Videos',		'created_at' => 'Time',		'fields' => [		],	],
		'video' => [		'title' => 'Video ',		'created_at' => 'Time',		'fields' => [			'book' => 'Book',			'name' => 'Name',			'embed-code' => 'Embed code',			'video-file' => 'Video file',			'video-thumbnail' => 'Video thumbnail',		],	],
		'user-actions' => [		'title' => 'User actions',		'created_at' => 'Time',		'fields' => [			'user' => 'User',			'action' => 'Action',			'action-model' => 'Action model',			'action-id' => 'Action id',		],	],
		'general-settings' => [		'title' => 'General Settings',		'created_at' => 'Time',		'fields' => [		],	],
		'districts' => [		'title' => 'Districts',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'code' => 'Code',		],	],
		'schools' => [		'title' => 'Schools',		'created_at' => 'Time',		'fields' => [			'district' => 'District',			'name' => 'Name',		],	],
		'book-chapters' => [		'title' => 'Book chapters',		'created_at' => 'Time',		'fields' => [			'chapter-title' => 'Chapter title',			'chapter-file' => 'Chapter file',			'book' => 'Book',		],	],
	'qa_create' => 'बनाइए (क्रिएट)',
	'qa_save' => 'सुरक्षित करे ',
	'qa_edit' => 'संपादित करे (एडिट)',
	'qa_view' => 'देखें',
	'qa_update' => 'सुधारे ',
	'qa_list' => 'सूची',
	'qa_no_entries_in_table' => 'टेबल मे एक भी एंट्री नही है ',
	'custom_controller_index' => 'विशेष(कस्टम) कंट्रोलर इंडेक्स ।',
	'qa_logout' => 'लोग आउट',
	'qa_add_new' => 'नया समाविष्ट करे',
	'qa_are_you_sure' => 'आप निस्चित है ?',
	'qa_back_to_list' => 'सूची पे वापस जाए',
	'qa_dashboard' => 'डॅशबोर्ड ',
	'qa_delete' => 'मिटाइए',
	'create' => 'बनाइए (क्रिएट)',
	'save' => 'सुरक्षित करे ',
	'edit' => 'संपादित करे (एडिट)',
	'view' => 'देखें',
	'update' => 'सुधारे ',
	'list' => 'सूची',
	'no_entries_in_table' => 'टेबल मे एक भी एंट्री नही है ',
	'logout' => 'लोग आउट',
	'add_new' => 'नया समाविष्ट करे',
	'are_you_sure' => 'आप निस्चित है ?',
	'back_to_list' => 'सूची पे वापस जाए',
	'dashboard' => 'डॅशबोर्ड ',
	'delete' => 'मिटाइए',
	'quickadmin_title' => 'DigitalBooks',
];