<?php

return [
		'user-management' => [		'title' => 'User Management',		'created_at' => 'Time',		'fields' => [		],	],
		'roles' => [		'title' => 'Roles',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',		],	],
		'users' => [		'title' => 'Users',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'email' => 'Email',			'password' => 'Password',			'role' => 'Role',			'remember-token' => 'Remember token',			'district' => 'District',			'school' => 'School',			'approved' => 'Approved',		],	],
		'books' => [		'title' => 'Books Module',		'created_at' => 'Time',		'fields' => [		],	],
		'books' => [		'title' => 'Books',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'cover' => 'Cover',			'description' => 'Description',			'type' => 'Type',			'teachers' => 'Teachers',		],	],
		'book-codes' => [		'title' => 'Book codes',		'created_at' => 'Time',		'fields' => [			'book' => 'Book',			'code' => 'Code',		],	],
		'tests' => [		'title' => 'Tests',		'created_at' => 'Time',		'fields' => [		],	],
		'test' => [		'title' => 'Test',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'description' => 'Description',			'chapter' => 'Chapter',			'book' => 'Book',		],	],
		'questions' => [		'title' => 'Questions',		'created_at' => 'Time',		'fields' => [			'test' => 'Test',			'question' => 'Question',		],	],
		'answers' => [		'title' => 'Answers',		'created_at' => 'Time',		'fields' => [			'question' => 'Question',			'answer' => 'Answer',			'correct-answer' => 'Correct answer',			'points' => 'Points',		],	],
		'videos' => [		'title' => 'Videos',		'created_at' => 'Time',		'fields' => [		],	],
		'video' => [		'title' => 'Video ',		'created_at' => 'Time',		'fields' => [			'book' => 'Book',			'name' => 'Name',			'embed-code' => 'Embed code',			'video-file' => 'Video file',			'video-thumbnail' => 'Video thumbnail',		],	],
		'user-actions' => [		'title' => 'User actions',		'created_at' => 'Time',		'fields' => [			'user' => 'User',			'action' => 'Action',			'action-model' => 'Action model',			'action-id' => 'Action id',		],	],
		'general-settings' => [		'title' => 'General Settings',		'created_at' => 'Time',		'fields' => [		],	],
		'districts' => [		'title' => 'Districts',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'code' => 'Code',		],	],
		'schools' => [		'title' => 'Schools',		'created_at' => 'Time',		'fields' => [			'district' => 'District',			'name' => 'Name',		],	],
		'book-chapters' => [		'title' => 'Book chapters',		'created_at' => 'Time',		'fields' => [			'chapter-title' => 'Chapter title',			'chapter-file' => 'Chapter file',			'book' => 'Book',		],	],
	'qa_save' => 'Išsaugoti',
	'qa_update' => 'Atnaujinti',
	'qa_list' => 'Sąrašas',
	'qa_no_entries_in_table' => 'Įrašų nėra.',
	'qa_create' => 'Sukurti',
	'qa_edit' => 'Redaguoti',
	'qa_view' => 'Peržiūrėti',
	'custom_controller_index' => 'Papildomo Controller\'io puslapis.',
	'qa_logout' => 'Atsijungti',
	'qa_add_new' => 'Pridėti naują',
	'qa_are_you_sure' => 'Ar esate tikri?',
	'qa_back_to_list' => 'Grįžti į sąrašą',
	'qa_dashboard' => 'Pagrindinis',
	'qa_delete' => 'Trinti',
	'save' => 'Išsaugoti',
	'update' => 'Atnaujinti',
	'list' => 'Sąrašas',
	'no_entries_in_table' => 'Įrašų nėra.',
	'create' => 'Sukurti',
	'edit' => 'Redaguoti',
	'view' => 'Peržiūrėti',
	'logout' => 'Atsijungti',
	'add_new' => 'Pridėti naują',
	'are_you_sure' => 'Ar esate tikri?',
	'back_to_list' => 'Grįžti į sąrašą',
	'dashboard' => 'Pagrindinis',
	'delete' => 'Trinti',
	'quickadmin_title' => 'DigitalBooks',
];