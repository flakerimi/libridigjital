<?php
Route::get('/', function () {
    return redirect('/home');
});

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

// Registration Routes..
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
$this->post('register', 'Auth\RegisterController@register')->name('auth.register');

Route::group(['middleware' => ['auth', 'approved']], function () {
    Route::get('books/{book_id}/chapters.json', function($book_id)
    {
        return App\BookChapter::where('book_id', $book_id)->select('id','chapter_title')->get();
    });
    Route::get('/home', 'HomeController@index');
    Route::get('/ndihma','HomeController@ndihma');
    Route::resource('roles', 'RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'UsersController');
    Route::post('users_mass_destroy', ['uses' => 'UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::get('books/shelf', ['uses' => 'BooksController@shelf', 'as' => 'books.shelf']);
    Route::get('books/shelf/{book}', ['uses' => 'BooksController@book', 'as' => 'books.book']);
    Route::get('books/redeem', ['uses' => 'BooksController@redeem', 'as' => 'books.redeem']);
    Route::get('tests/take/{id}', ['uses' => 'TestsController@take', 'as' => 'tests.take']);
    Route::get('test_results/{user_id}/test/{id}', ['uses' => 'TestResultsController@view', 'as' => 'testsresult.view']);
    Route::post('tests/taking/{id}', ['uses' => 'TestResultsController@taking', 'as' => 'testsresult.take']);
    Route::resource('books', 'BooksController');
    Route::post('books_mass_destroy', ['uses' => 'BooksController@massDestroy', 'as' => 'books.mass_destroy']);
    Route::resource('book_codes', 'BookCodesController');
    Route::post('book_codes_mass_destroy', ['uses' => 'BookCodesController@massDestroy', 'as' => 'book_codes.mass_destroy']);
    Route::resource('tests', 'TestsController');
    Route::post('tests_mass_destroy', ['uses' => 'TestsController@massDestroy', 'as' => 'tests.mass_destroy']);
    Route::resource('questions', 'QuestionsController');
    Route::post('questions_mass_destroy', ['uses' => 'QuestionsController@massDestroy', 'as' => 'questions.mass_destroy']);
    Route::resource('answers', 'AnswersController');
    Route::post('answers_mass_destroy', ['uses' => 'AnswersController@massDestroy', 'as' => 'answers.mass_destroy']);
    Route::resource('videos', 'VideosController');
    Route::post('videos_mass_destroy', ['uses' => 'VideosController@massDestroy', 'as' => 'videos.mass_destroy']);
    Route::resource('user_actions', 'UserActionsController');
    Route::resource('districts', 'DistrictsController');
    Route::post('districts_mass_destroy', ['uses' => 'DistrictsController@massDestroy', 'as' => 'districts.mass_destroy']);
    Route::resource('schools', 'SchoolsController');
    Route::post('schools_mass_destroy', ['uses' => 'SchoolsController@massDestroy', 'as' => 'schools.mass_destroy']);
    Route::get('book_chapters/chapter/{id}', 'BookChaptersController@chapter');
    Route::resource('book_chapters', 'BookChaptersController');
    Route::post('book_chapters_mass_destroy', ['uses' => 'BookChaptersController@massDestroy', 'as' => 'book_chapters.mass_destroy']);

    Route::model('messenger', 'App\MessengerTopic');
    Route::get('messenger/inbox', 'MessengerController@inbox')->name('messenger.inbox');
    Route::get('messenger/outbox', 'MessengerController@outbox')->name('messenger.outbox');
    Route::resource('messenger', 'MessengerController');
});
