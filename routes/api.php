<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {

        Route::resource('book_codes', 'BookCodesController');

        Route::resource('answers', 'AnswersController');

        Route::resource('book_chapters', 'BookChaptersController');

});
