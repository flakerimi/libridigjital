<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('book_codes')) {
            Schema::create('book_codes', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('book_id')->unsigned()->nullable();
                $table->foreign('book_id', 'fk_11575_book_book_id_book_code')->references('id')->on('books')->onDelete('cascade');
                $table->string('code')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_codes');
    }
}
