<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropBookTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('book_test');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('book_test')) {
            Schema::create('book_test', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('book_id')->unsigned()->nullable();
            $table->foreign('book_id', 'fk_11575_book_test_id')->references('id')->on('books');
                $table->integer('test_id')->unsigned()->nullable();
            $table->foreign('test_id', 'fk_11578_test_book_id')->references('id')->on('tests');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
