<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1487241312UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('district_id')->unsigned()->nullable();
                $table->foreign('district_id', 'fk_15457_district_district_id_user')->references('id')->on('districts')->onDelete('cascade');
                $table->integer('school_id')->unsigned()->nullable();
                $table->foreign('school_id', 'fk_15458_school_school_id_user')->references('id')->on('schools')->onDelete('cascade');
                
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('fk_15457_district_district_id_user');
            $table->dropIndex('fk_15457_district_district_id_user');
            $table->dropColumn('district_id');
            $table->dropForeign('fk_15458_school_school_id_user');
            $table->dropIndex('fk_15458_school_school_id_user');
            $table->dropColumn('school_id');
            
        });

    }
}
