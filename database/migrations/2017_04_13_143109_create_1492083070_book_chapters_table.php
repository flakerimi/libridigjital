<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1492083070BookChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('book_chapters')) {
            Schema::create('book_chapters', function (Blueprint $table) {
                $table->increments('id');
                $table->string('chapter_title')->nullable();
                $table->string('chapter_file')->nullable();
                $table->integer('book_id')->unsigned()->nullable();
                $table->foreign('book_id', '29546_58ef617d07188')->references('id')->on('books')->onDelete('cascade');
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_chapters');
    }
}
