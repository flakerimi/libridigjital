<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1486502152QuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->integer('test_id')->unsigned()->nullable();
                $table->foreign('test_id', 'fk_11578_test_test_id_question')->references('id')->on('tests')->onDelete('cascade');
                
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->dropForeign('fk_11578_test_test_id_question');
            $table->dropIndex('fk_11578_test_test_id_question');
            $table->dropColumn('test_id');
            
        });

    }
}
