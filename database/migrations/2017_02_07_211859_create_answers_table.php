<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('answers')) {
            Schema::create('answers', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('question_id')->unsigned()->nullable();
                $table->integer('test_id')->unsigned()->nullable();
                $table->foreign('question_id', 'fk_11579_question_question_id_answer')->references('id')->on('questions')->onDelete('cascade');
                $table->string('answer')->nullable();
                $table->enum('correct_answer', ["Yes","No"])->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
