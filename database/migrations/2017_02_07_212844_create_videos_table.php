<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('videos')) {
            Schema::create('videos', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('book_id')->unsigned()->nullable();
                $table->integer('chapter_id')->unsigned()->nullable();
                $table->foreign('book_id', 'fk_11575_book_book_id_video')->references('id')->on('books')->onDelete('cascade');
                $table->string('name')->nullable();
                $table->text('embed_code')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
