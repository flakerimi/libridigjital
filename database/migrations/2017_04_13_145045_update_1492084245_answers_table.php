<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1492084245AnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
            
        });
Schema::table('answers', function (Blueprint $table) {
            $table->integer('points')->nullable()->unsigned();
                
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->dropColumn('points');
            
        });
Schema::table('answers', function (Blueprint $table) {
            $table->softDeletes();
            
        });

    }
}
