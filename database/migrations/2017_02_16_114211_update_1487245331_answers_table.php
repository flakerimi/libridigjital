<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1487245331AnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->dropColumn('correct_answer');
            
        });
Schema::table('answers', function (Blueprint $table) {
            $table->tinyInteger('correct_answer')->nullable()->default(0);
                
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->dropColumn('correct_answer');
            
        });
Schema::table('answers', function (Blueprint $table) {
                        $table->enum('correct_answer', ["Yes","No"])->nullable();
                
        });

    }
}
