<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1492267317TestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tests', function (Blueprint $table) {
            $table->integer('chapter_id')->unsigned()->nullable();
                $table->foreign('chapter_id', '11578_58f23133bb139')->references('id')->on('book_chapters')->onDelete('cascade');
                $table->integer('book_id')->unsigned()->nullable();
                $table->foreign('book_id', '11578_58f23133c33b7')->references('id')->on('books')->onDelete('cascade');
                
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tests', function (Blueprint $table) {
            $table->dropForeign('11578_58f23133bb139');
            $table->dropIndex('11578_58f23133bb139');
            $table->dropColumn('chapter_id');
            $table->dropForeign('11578_58f23133c33b7');
            $table->dropIndex('11578_58f23133c33b7');
            $table->dropColumn('book_id');
            
        });

    }
}
