<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'name' => 'Admin', 'email' => 'admin@admin.com', 'password' => '$2y$10$KjqL8KVgKdqZJJ60MMBSreJdx320tM8NlUymL0KGqz/NWFAOoqCCe', 'role_id' => 1, 'remember_token' => '', 'district_id' => null, 'school_id' => null, 'approved' => 1,],
            ['id' => 2, 'name' => 'Mesues Godo', 'email' => 'teacher@mediaprint.al', 'password' => '$2y$10$LflTYy5u6WftW2qAdn9RPOBU1sWVDaxTtRREMPvljUF7MkHNIJAy.', 'role_id' => 3, 'remember_token' => null, 'district_id' => null, 'school_id' => null, 'approved' => 1,],
            ['id' => 3, 'name' => 'Nxenes i Shkolles', 'email' => 'nexenes@mediaprint.al', 'password' => '$2y$10$qzKsqK7pEwXJTBf/XKi9MOiGhedd1EDbzd4PzQ2N9u1tvjshT0n8u', 'role_id' => 4, 'remember_token' => null, 'district_id' => null, 'school_id' => null, 'approved' => 0,],

        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
